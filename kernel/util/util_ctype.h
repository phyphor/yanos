#ifndef UTIL_CTYPE_H_
#define UTIL_CTYPE_H_

#include <stdbool.h>

static bool isdigit(int ch)
{
	return ch >= '0' && ch <= '9';
}

static char tolower(char ch)
{
	if(ch >= 'A' && ch <= 'Z')
		return ch - 'A' + 'a';
	return ch;
}

static char toupper(char ch)
{
	if(ch >= 'a' && ch <= 'z')
		return ch - 'a' + 'A';
	return ch;
}

#endif
