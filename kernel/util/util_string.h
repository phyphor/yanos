#ifndef UTIL_STRING_H_
#define UTIL_STRING_H_

#ifdef __GNUC__
#define restrict __restrict
#endif

#if !defined(__cplusplus) && !defined(nullptr)
#define nullptr 0
#endif

#if IS_HOSTED
#include <string.h>
#else
#include <stddef.h>

// Basic naive implementations of these functions.
#ifndef HAVE_MEMCMP
static int memcmp(const void *restrict a, const void *restrict b, size_t sz)
{
	const char *dst = (char*)a;
	const char *src = (char*)b;
	for(size_t i = 0; i < sz; i++) {
		int diff = dst[i] - src[i];
		if (diff != 0)
			return diff;
	}
	return 0;
}
#endif

#ifndef HAVE_MEMSET
static void* memset(void *restrict a, size_t val, size_t sz)
{
	char *dst = (char*)a;
	for(size_t i = 0; i < sz; i++) {
		dst[i] = (char)val;
	}
	return a;
}
#endif

#ifndef HAVE_MEMMOVE
static void* memmove(void *a, const void *b, size_t sz)
{
	char *dst = (char*)a;
	const char *src = (char*)b;
	for(size_t i = 0; i < sz; i++) {
		dst[i] = src[i];
	}
	return a;
}
#endif

#ifndef HAVE_MEMCPY
static void* memcpy(void *restrict a, const void *restrict b, size_t sz)
{
	return memmove(a, b, sz);
}
#endif

#ifndef HAVE_STRLEN
static size_t strlen(const char *s)
{
	const char *e = s;
	while(*e)
		e++;
	return e - s;
}
#endif

#ifndef HAVE_STRCHR
static char* strchr(const char *s, char ch)
{
	for(; *s; s++) {
		if(*s == ch)
			return (char*)s;
	}
	return nullptr;
}
#endif

#endif

#endif
