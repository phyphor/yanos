#ifndef KPRINT_H_
#define KPRINT_H_

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif

// Print formatted text to the current default implementation
void kprintf(const char *fmt, ...);
void kvprintf(const char *fmt, va_list varg);

// Print formatted text to the given sized string
void kvsprintf(char *out, size_t sz, const char *fmt, va_list varg);
void ksprintf(char *out, size_t sz, const char *fmt, ...);

// Print using a custom puts function
void kvprint_impl(void (*puts)(void*, const char *str, size_t sz), void *context, const char *fmt, va_list varg);
void kprint_impl(void (*puts)(void*, const char *str, size_t sz), void *context, const char *fmt, ...);

// Initialize kprintf. _lock will be called before any print operation, _unlock will be called after.
void kprint_init(void *kprint_context,
	void (*kprint_lock)(void*),
	void (*kprint_unlock)(void*),
	void (*kprint_puts)(void*,const char*,size_t));

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
// Allow safe bare printing to arrays from C++
template<size_t N>
void ksprintf(char (&out)[N], const char *fmt, ...)
{
	va_list varg;
	va_start(varg, fmt);
	kvsprintf(out, N, fmt, varg);
	va_end(varg);
}

template<size_t N>
void kvsprintf(char (&out)[N], const char *fmt, va_list varg)
{
	kvsprintf(out, N, fmt, varg);
}
#endif

#endif
