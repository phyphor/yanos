#include "kprint.h"

#include "util_ctype.h"
#include "util_string.h"

static void *kprint_context;
static void (*kprint_lock)(void*);
static void (*kprint_unlock)(void*);
static void (*kprint_puts)(void*,const char*,size_t);

static const char *hex_chars = "0123456789ABCDEF";
static const char *zeros = "0000000000000000000000000000000000000000000000000000000000000000";

void kprint_init(void *kprint_context_,
		void (*kprint_lock_)(void*),
		void (*kprint_unlock_)(void*),
		void (*kprint_puts_)(void*,const char*,size_t)) {
	kprint_lock = kprint_lock_;
	kprint_unlock = kprint_unlock_;
	kprint_puts = kprint_puts_;
	kprint_context = kprint_context_;
}

static size_t itoa(char *str, int64_t value, int base)
{
	char *actual_start = str;
	if (value == 0) {
		*str++ = '0';
		return 1;
	}
	if (value < 0) {
		*str++ = '-';
		value = -value;
	}
	char *start = str;
	for(; value != 0; value /= base)
		*str++ = hex_chars[value % base];

	size_t sz = str - actual_start;
	str--;
	while(str > start) {
		char ch = *str;
		*str = *start;
		*start = ch;
		str--, start++;
	}
	return sz;
}

static size_t utoa(char *str, uint64_t value, int base)
{
	if (value == 0) {
		*str++ = '0';
		return 1;
	}
	char *start = str;
	for(; value != 0; value /= base)
		*str++ = hex_chars[value % base];

	size_t sz = str - start;
	str--;
	while(str > start) {
		char ch = *str;
		*str = *start;
		*start = ch;
		str--, start++;
	}
	return sz;
}

void kvprint_impl(void (*puts)(void*, const char *str, size_t sz), void *context, const char *str, va_list varg)
{
	char temp[24];
	while(*str) {
		if(*str != '%') {
			const char *start = str++;
			while(*str && *str != '%')
				str++;

			puts(context, start, str - start);
			continue;
		}
		str++;
		if(*str == '%') {
			str++;
			puts(context, "%", 1);
			break;
		}
		bool zeroprefix = (*str == '0');
		size_t width = 0;
		while(isdigit(*str))
			width = width * 10 + *str++ - '0';
		union {
			int64_t i64;
			uint64_t u64;
			void *ptr;
		} u;

		bool negative = false;

		if(*str == 'l') {
			u.u64 = va_arg(varg, unsigned long);
			str++;
		} else if(*str == 'p' || *str == 's') {
			u.ptr = va_arg(varg, void*);
		} else {
			u.u64 = va_arg(varg, unsigned int);
			if(u.u64 & 0x80000000)
				negative = true;
		}

		switch(*str) {
		case 's': {
			const char *string = (const char*)u.ptr;
			if(string)
				puts(context, string, strlen(string));
			else
				puts(context, "(null)", 6);
			} break;
		case 'd':
		case 'i': {
			if(negative)
				u.i64 -= 0x100000000LL;
			size_t len = itoa(temp, u.i64, 10);
			if(width > len)
				puts(context, zeros, width - len);
			puts(context, temp, len);
			} break;
		case 'u': {
			size_t len = utoa(temp, u.u64, 10);
			if(width > len)
				puts(context, zeros, width - len);
			puts(context, temp, len);
			} break;
		case 'p':
			width = 2 * sizeof(void*);
			zeroprefix = true;
		case 'x':
		case 'X': {
			size_t len = utoa(temp, u.u64, 16);
			if(width > len)
				puts(context, zeros, width - len);
			if(*str == 'x') {
				for(size_t i = 0; i < len; i++)
					temp[i] = tolower(temp[i]);
			}
			puts(context, temp, len);
			} break;
		case 'c': {
			char ch = (char)u.u64;
			puts(context, &ch, 1);
			} break;
		default:
			continue;
		}
		str++;
		continue;
	}
}

void kprint_impl(void (*puts)(void*, const char *str, size_t sz), void *context, const char *fmt, ...)
{
	va_list varg;
	va_start(varg, fmt);
	kvprint_impl(puts, context, fmt, varg);
	va_end(varg);
}

void kvprintf(const char *fmt, va_list varg)
{
	kprint_lock(kprint_context);
	kvprint_impl(kprint_puts, kprint_context, fmt, varg);
	kprint_unlock(kprint_context);
}

void kprintf(const char *fmt, ...)
{
	va_list varg;
	va_start(varg, fmt);
	kvprintf(fmt, varg);
	va_end(varg);
}

typedef struct {
	char *current;
	char *end;
} ksprintf_info;

static void ksprintf_puts(void *context, const char *str, size_t sz)
{
	ksprintf_info *info = context;
	size_t remaining = info->end - info->current;
	if(remaining < sz)
		sz = remaining;
	memcpy(info->current, str, sz);
	info->current += sz;
}

void kvsprintf(char *out, size_t sz, const char *fmt, va_list varg)
{
	ksprintf_info info = { out, out+sz-1 };
	kvprint_impl(&ksprintf_puts, &info, fmt, varg);
	*info.current = '\0';
}

void ksprintf(char *out, size_t sz, const char *fmt, ...)
{
	va_list varg;
	va_start(varg, fmt);
	kvsprintf(out, sz, fmt, varg);
	va_end(varg);
}
