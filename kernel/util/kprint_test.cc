#include "kprint.h"

#include "gtest/gtest.h"

TEST(kprintTest, Formatting) {
	char ar[128];
	ksprintf(ar, "test %d %s %u %08X", -1, "foo", 0x7FFFFFFF, 0x1234);
	EXPECT_EQ(ar, std::string("test -1 foo 2147483647 00001234"));
}

TEST(kprintTest, MaxLength) {
	union {;
		char ar[32];
		char ar2[256];
	};
	ksprintf(ar, "0123456789012345678901234567890123456789");
	EXPECT_EQ(ar, std::string("0123456789012345678901234567890"));

	ksprintf(ar, "%s %s %s", "0123456789", "0123456789", "012345678");
	EXPECT_EQ(ar, std::string("0123456789 0123456789 012345678"));

	ksprintf(ar, "%s %s %s %08X", "0123456789", "0123456789", "0123", 0x12345678);
	EXPECT_EQ(ar, std::string("0123456789 0123456789 0123 1234"));
}
