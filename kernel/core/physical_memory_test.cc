#include "gtest/gtest.h"

#include "arch/physical_memory.h"
#include "arch/hosted.h"
#include "core/physical_memory.h"

namespace {

class PhysMemTest : public ::testing::Test
{
public:
	void InitMemory(size_t size, bool set_to_junk, bool assume_clean = false)
	{
		// We cannot assume clean if we initialize memory - the allocator will have garbage data in it
		HostInitializePhysicalMemory(size, set_to_junk, !set_to_junk && assume_clean);
	}
};

TEST_F(PhysMemTest, Initialize)
{
	InitMemory(1 << 24, true);
}

TEST_F(PhysMemTest, AllocateAll)
{
	// Don't initialize the memory, assume clean (because it is)
	InitMemory(1 << 30, false, true);

	std::set<pfn_t> pfn_set;
	std::vector<pfn_t> pfns;

	pfns.reserve(10000);

	// First, allocate all memory
	for(;;) {
		pfn_t pfn = MemAllocPhysPage();
		if(!pfn)
			break;
		EXPECT_TRUE(pfn_set.insert(pfn).second);
		pfns.push_back(pfn);
	}

	size_t count = pfns.size();

	// We have a full set of pfns, free them in random order
	std::random_shuffle(pfns.begin(), pfns.end());
	for(auto pfn: pfns)
		MemFreePhysPage(pfn);

	pfns.clear();
	pfn_set.clear();

	for(;;) {
		pfn_t pfn = MemAllocPhysPage();
		if(!pfn)
			break;
		EXPECT_TRUE(pfn_set.insert(pfn).second);
		pfns.push_back(pfn);
	}

	// We should be able to re-allocate all of these pages
	EXPECT_EQ(count, pfns.size());
}

}
