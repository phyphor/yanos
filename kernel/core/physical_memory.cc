#include "physical_memory.h"
#include "arch/memory_map.h"
#include "arch/physical_memory.h"
#include "util/util_string.h"

namespace {

// Physical memory is generally always going to be in use by some part of the system, so it will be stored
// in multiple lists.
// Zero list - memory that is known to be zeroed. This is ready for immediate use
// Free list - memory that has been released but is or may be dirty. This can be cleaned for immediate use
// Available list - memory that is in use but can be discarded and claimed for other use, eg file cache

// A two-level allocator is used. Each processor keeps a page filled with clean pages and a page filled with
// dirty pages. When these pages fill up or empty, additional entries are pulled from/pushed to the main
// lists. Because the processor-specifc lists are only ever acccessed by one processor it is sufficient
// to simply disable interrupts to ensure atomicity and synchronization. This allows for fast allocation
// in common cases. Each page contains typically a few megabytes at most of data, so it shouldn't cause
// significant amounts of waste even in the worse case. The main lists are protected by a spinlock, however
// it only needs to be held long enough to enqueue or dequeue a page. Contention should be low because the
// main list need only be touched every few hundred operations typically

// This contains up to |n| pfns with a standard FILO stack
template<size_t n_>
struct PfnListStruct
{
	static constexpr size_t max_size = n_;

	PfnListStruct() : next_pfn(0), valid_count(0) {}

	bool Push(pfn_t pfn)
	{
		if(valid_count == max_size)
			return false;
		pfns[valid_count++] = pfn;
		return true;
	}

	pfn_t Pop()
	{
		if(!valid_count)
			return 0;
		pfn_t ret = pfns[--valid_count];
		pfns[valid_count] = 0;
		return ret;
	}

	bool full() const { return valid_count == max_size; }

	pfn_t next_pfn;
	pfn_t valid_count; // Same type is used to ensure packing
	pfn_t pfns[max_size];
};

constexpr size_t kPfnsPerPage = kPageSize / sizeof(pfn_t) - 2;
constexpr size_t kMaxPageMemory = 16 << 20;
constexpr size_t kMaxPfnsPerPage = kMaxPageMemory / kPageSize;
constexpr size_t GetPfnsPerPage()
{
	return kPfnsPerPage > kMaxPfnsPerPage ? kMaxPfnsPerPage : kPfnsPerPage;
}

// This is sized to occupy exactly one page, which is convenient to split the list up because each
// list entry is also exactly one page
typedef PfnListStruct<GetPfnsPerPage()> PfnListPage;
static_assert(sizeof(PfnListPage) <= kPageSize, "PfnListPage is too large");

// This consists of a list of physical pages, each containing some number of free pfns.
// This could operate as a stack or queue, currently it is a queue
class PfnList
{
public:
	// Pushes the page |pfn| to the list
	void PushList(pfn_t pfn, PfnListPage *mapped_ptr)
	{
		if(tail_) {
			tail_ptr_->next_pfn = pfn;
			ArchUnmapTemporaryPfn(tail_ptr_, tail_);
		} else {
			head_ = pfn;
		}
		tail_ = pfn;
		tail_ptr_ = mapped_ptr;
	}

	// On failure, returns 0
	pfn_t PopList(PfnListPage **out_page)
	{
		if(!head_)
			return 0;

		pfn_t ret = head_;

		PfnListPage *page = ArchMapTemporaryPfn<PfnListPage>(ret);

		if(head_ == tail_) {
			head_ = tail_ = 0;
			tail_ptr_ = nullptr;
		} else {
			head_ = page->next_pfn;
		}
		page->next_pfn = 0;
		*out_page = page;
		return ret;
	}

	void Add(pfn_t start, pfn_t end)
	{
		if(!tail_) {
			head_ = tail_ = start;
			tail_ptr_ = ArchMapTemporaryPfn<PfnListPage>(start);
			memset(tail_ptr_, 0, sizeof(PfnListPage));
			start++;
		}
		while(start < end) {
			if(tail_ptr_->full()) {
				tail_ptr_->next_pfn = start;
				ArchUnmapTemporaryPfn(tail_ptr_, tail_);

				tail_ = start;
				tail_ptr_ = ArchMapTemporaryPfn<PfnListPage>(tail_);
				memset(tail_ptr_, 0, sizeof(PfnListPage));
				start++;
			} else {
				tail_ptr_->Push(start++);
			}
		}
	}

private:
	pfn_t head_ = 0;
	pfn_t tail_ = 0;
	PfnListPage *tail_ptr_ = nullptr;
};

void FillListFromInUse(PfnListPage *dirty_page)
{
	// Just a stub for now
}

}

struct PerProcessorPhysicalAllocator
{
	void Initialize(PfnList *zero_list, PfnList *free_list)
	{
		zero_pages_ = zero_list;
		free_pages_ = free_list;

		free_pfn_ = free_list->PopList(&free_);
		zero_pfn_ = zero_list->PopList(&zero_);
		if(!free_pfn_ && !zero_pfn_) {
		} else if(!free_pfn_) {
			free_pfn_ = zero_->Pop();
			free_ = ArchMapTemporaryPfn<PfnListPage>(free_pfn_);
		} else if(!zero_pfn_) {
			zero_pfn_ = free_->Pop();
			zero_ = ArchMapTemporaryPfn<PfnListPage>(zero_pfn_);
			memset(zero_, 0, sizeof(PfnListPage));
		}
	}

	inline pfn_t TryAllocDirty()
	{
		return free_->Pop();
	}

	inline pfn_t TryAllocFromCache()
	{
		return zero_->Pop();
	}

	pfn_t TryAlloc()
	{
		pfn_t pfn = TryAllocFromCache();
		if(pfn)
			return pfn;

		PfnListPage *new_list;
		pfn_t new_page = zero_pages_->PopList(&new_list);
		if(new_page) {
			// This is safe because Pop() processively zeros the page, so an empty cache means a zero page
			pfn = zero_pfn_;
			ArchUnmapTemporaryPfn(zero_, zero_pfn_);
			zero_pfn_ = new_page;
			zero_ = new_list;
			return pfn;
		}

		return SlowAlloc();
	}

	pfn_t SlowAlloc()
	{
		pfn_t pfn = TryAllocDirty();
		if(pfn) {
			// Must clean
			Clean(pfn);
			return pfn;
		}

		// The clean list is empty and the dirty cache is also empty. Try the main dirty list
		PfnListPage *new_list;
		pfn_t new_page = free_pages_->PopList(&new_list);
		if(new_page) {
			// This is actually clean, as per TryAlloc
			pfn = free_pfn_;
			ArchUnmapTemporaryPfn(free_, free_pfn_);
			free_pfn_ = new_page;
			free_ = new_list;
			return pfn;
		}

		// Okay, no unused memory. Try to pull some from file caches and whatnot
		FillListFromInUse(free_);
		pfn = TryAllocDirty();
		if(pfn) {
			Clean(pfn);
			return pfn;
		}
		return 0; // Out of memory!
	}

	void Free(pfn_t pfn)
	{
		if(pfn && !free_->Push(pfn)) {
			// This is full now, so enqueue it
			free_pages_->PushList(free_pfn_, free_);
			pfn_t clean = TryAllocFromCache();
			if(clean) {
				free_ = ArchMapTemporaryPfn<PfnListPage>(clean);
				free_pfn_ = clean;
				free_->Push(pfn);
			} else {
				free_ = ArchMapTemporaryPfn<PfnListPage>(pfn);
				free_pfn_ = pfn;
				memset(free_, 0, sizeof(PfnListPage));
			}
		}
	}

	void FreeClean(pfn_t pfn)
	{
		if(pfn && !zero_->Push(pfn)) {
			// This is full now, so enqueue it
			zero_pages_->PushList(zero_pfn_, zero_);
			zero_pfn_ = pfn;
			zero_ = ArchMapTemporaryPfn<PfnListPage>(pfn);
		}
	}

	static PerProcessorPhysicalAllocator* Get();

private:
	void Clean(pfn_t pfn)
	{
		void *ptr = ArchMapTemporaryPfn(pfn);
		memset(ptr, 0, kPageSize);
		ArchUnmapTemporaryPfn(ptr, pfn);
	}

	pfn_t zero_pfn_;
	PfnListPage *zero_;
	pfn_t free_pfn_;
	PfnListPage *free_;
	PfnList *zero_pages_;
	PfnList *free_pages_;
};

struct {
	PfnList FreeList;
	PfnList ZeroList;
	PerProcessorPhysicalAllocator cpu0_PhysicalAllocator;
} PhysicalMemoryAllocatorState;

PerProcessorPhysicalAllocator* PerProcessorPhysicalAllocator::Get()
{
	return &PhysicalMemoryAllocatorState.cpu0_PhysicalAllocator;
}


void MemAddMemory(pfn_t start, pfn_t end, bool assume_clean)
{
	if(assume_clean)
		PhysicalMemoryAllocatorState.ZeroList.Add(start, end);
	else
		PhysicalMemoryAllocatorState.FreeList.Add(start, end);
}

void MemInit()
{
	PerProcessorPhysicalAllocator::Get()->Initialize(&PhysicalMemoryAllocatorState.ZeroList, &PhysicalMemoryAllocatorState.FreeList);
}

void MemCleanup()
{
	memset(&PhysicalMemoryAllocatorState, 0, sizeof(PhysicalMemoryAllocatorState));
}

pfn_t MemAllocPhysPageForMapping()
{
	return MemAllocPhysPage();
}

pfn_t MemAllocPhysPageFromInterrupt()
{
	return MemAllocPhysPage();
}

pfn_t MemAllocPhysPage()
{
	ScopedInterruptDisable int_disable;
	return PerProcessorPhysicalAllocator::Get()->TryAlloc();
}

size_t MemAllocPhysPages(pfn_t *pfns, size_t count)
{
	ScopedInterruptDisable int_disable;
	auto *alloc = PerProcessorPhysicalAllocator::Get();
	size_t n = 0;
	for(n = 0; n < count; n++) {
		if(!(pfns[n] = alloc->TryAlloc()))
			break;
	}
	return n;
}

pfn_t MemAllocPhysPageDirty(bool *is_clean)
{
	ScopedInterruptDisable int_disable;
	auto *alloc = PerProcessorPhysicalAllocator::Get();
	pfn_t pfn = alloc->TryAllocDirty();
	if(pfn) {
		if(is_clean)
			*is_clean = false;
		return pfn;
	}
	if(is_clean)
		*is_clean = true;
	return alloc->TryAlloc();
}

void MemFreePhysPage(pfn_t pfn)
{
	ScopedInterruptDisable int_disable;
	PerProcessorPhysicalAllocator::Get()->Free(pfn);
}

void MemFreePhysPages(const pfn_t *pfns, size_t count)
{
	ScopedInterruptDisable int_disable;
	auto *alloc = PerProcessorPhysicalAllocator::Get();
	for(size_t i = 0; i < count; i++)
		alloc->Free(pfns[i]);
}

void MemFreeCleanPhysPages(const pfn_t *pfns, size_t count)
{
	ScopedInterruptDisable int_disable;
	auto *alloc = PerProcessorPhysicalAllocator::Get();
	for(size_t i = 0; i < count; i++)
		alloc->FreeClean(pfns[i]);
}
