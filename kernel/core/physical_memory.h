#ifndef CORE_PHYSICAL_MEMORY_H
#define CORE_PHYSICAL_MEMORY_H

#include "arch/config.h"

// PFN (Page Frame Number)
// Each virtual address consists of kPageShift bits of offset, with the remaining forming the PFN.
// PFNs always refer to the page in its entirety, while physical addresses refer to specific bytes

struct PFN;
struct PhysicalAddress;

struct PhysicalAddress
{
#ifdef __cplusplus
	PhysicalAddress(PFN pfn);
	PhysicalAddress(physical_addr_t addr) : physaddr(addr) {}

	PhysicalAddress operator += (size_t addr) { physaddr += addr; return *this; }
#endif

	physical_addr_t physaddr;
};

struct PFN
{
#ifdef __cplusplus
	PFN(PhysicalAddress addr);
	PFN(pfn_t pfn) : pfn(pfn) {}
#endif

	pfn_t pfn;
};

#ifdef __cplusplus
inline PhysicalAddress::PhysicalAddress(PFN pfn) : physaddr(static_cast<physical_addr_t>(pfn.pfn) << kPageShift) {}

inline PFN::PFN(PhysicalAddress addr) : pfn((pfn_t)(addr.physaddr >> kPageShift)) {}
#endif

void MemInit();

// Restores the physical memory allocator to its default state. Used in tests
void MemCleanup();

void MemAddMemory(pfn_t start, pfn_t end, bool assume_clean);

// This is guaranteed to be able to provide a few pages for use by the page mapping system without blocking or
// acquiring any locks that would be in use by the mapping subsystem. In particular, it must not trigger any
// mapping calls that would themselves require allocation.
// It is assumed that replacing the page already mapped at a given VA will always work
pfn_t MemAllocPhysPageForMapping();

// This is similar to MemAllocPhysPageForMapping but allocates a page in ISR context
pfn_t MemAllocPhysPageFromInterrupt();

// Allocate a single page. The page is always cleaned - cleaning may occur inline if needed
pfn_t MemAllocPhysPage();

// Attempt to allocate up to |count| pages. Returns the number of pages allocated.
size_t MemAllocPhysPages(pfn_t *pfns, size_t count);

// Allocate a page that may be dirty. Dirty pages may contain *any* data so should only be used if guaranteed to
// 1. be used in the kernel only
// 2. be entirely overwritten by the time usermode can read it
// If valid, |is_clean| recieves the clean/dirty state.
pfn_t MemAllocPhysPageDirty(bool *is_clean);

void MemFreePhysPage(pfn_t pfn);
void MemFreePhysPages(const pfn_t *pfns, size_t count);

void MemFreeCleanPhysPages(const pfn_t *pfns, size_t count);

#endif
