#ifndef X86_UTIL_H_
#define X86_UTIL_H_

#include <stdbool.h>
#include <stdint.h>

#ifndef BIT
#define BIT(x) (1ULL << (x))
#endif

#define kMSR_GSBase 0xC0000101
#define kMSR_EFER 0xC0000080

static uint64_t ReadCR3()
{
	uint64_t val;
	__asm("mov %%cr3, %0":"=r"(val));
	return val;
}

static uint64_t WriteCR3(uint64_t val)
{
	__asm("mov %0, %%cr3"::"r"(val):"memory");
	return val;
}

static void wrmsr64(uint32_t msr, uint64_t value)
{
	__asm("shr $32, %%rdx; wrmsr"::"c"(msr), "d"(value), "a"(value));
}

static void wrmsr32(uint32_t msr, uint32_t value)
{
	__asm("wrmsr"::"c"(msr), "d"(0), "a"(value));
}

static uint64_t rdmsr64(uint32_t msr)
{
	uint64_t value;
	__asm("rdmsr; shl $32, %%rdx; or %%rdx, %%rax":"=a"(value) : "c"(msr) : "rdx");
	return value;
}

static uint64_t rdmsr32(uint32_t msr)
{
	uint32_t value;
	__asm("rdmsr":"=a"(value) : "c"(msr) : "rdx");
	return value;
}

static bool cpuid_ecx(uint32_t eax, uint32_t bit)
{
	uint32_t v;
	__asm("cpuid":"=c"(v):"a"(eax):"rbx","rdx");
	return (v & (1 << bit)) != 0;
}

static bool cpuid_ebx(uint32_t eax, uint32_t bit)
{
	uint32_t v;
	__asm("cpuid":"=b"(v):"a"(eax):"rcx","rdx");
	return (v & (1 << bit)) != 0;
}
static bool cpuid_edx(uint32_t eax, uint32_t bit)
{
	uint32_t v;
	__asm("cpuid":"=d"(v):"a"(eax):"rbx","rcx");
	return (v & (1 << bit)) != 0;
}

static bool cpuid_eax(uint32_t eax, uint32_t bit)
{
	uint32_t v;
	__asm("cpuid":"=a"(v):"a"(eax):"rbx","rdx","rcx");
	return (v & (1 << bit)) != 0;
}

#endif
