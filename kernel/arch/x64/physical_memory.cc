#include "arch/physical_memory.h"
#include "physical_memory.h"

#define PML4_SHIFT 39
#define PML4_OFFSET(va) ((va >> PML4_SHIFT) & 0x1FF)
#define PDP_SHIFT 30
#define PDP_OFFSET(va) ((va >> PDP_SHIFT) & 0x1FF)
#define PD_SHIFT 21
#define PD_OFFSET(va) ((va >> PD_SHIFT) & 0x1FF)
#define PT_SHIFT 12
#define PT_OFFSET(va) ((va >> PT_SHIFT) & 0x1FF)

#define kPteAddrMask 0x000FFFFFFFFFF000
#define kPteValid 1

extern uint64_t KernelCR3;

inline uint64_t AllocNewPageTableEntry()
{
	return (MemAllocPhysPageForMapping() << kPageShift) | kPteValid | kMemKernelReadWriteExec;
}

template<size_t shift>
uint64_t LookupAndAllocate(uint64_t addr, uint64_t va)
{
	uint64_t *table = (uint64_t*)ArchGetPhysicalPointer(addr);
	uint64_t e = table[(va >> shift) & 0x1FF];
	if((e & kPteValid)) {
		return e & kPteAddrMask;
	} else {
		return AllocNewPageTableEntry();
	}
}

pfn_t ArchMapPageInAddressSpace(physical_addr_t root, uint64_t va, uint64_t value)
{
	uint64_t addr = LookupAndAllocate<PD_SHIFT>(LookupAndAllocate<PDP_SHIFT>(
		LookupAndAllocate<PML4_SHIFT>(root, va), va), va);

	uint64_t *pt = (uint64_t*)ArchGetPhysicalPointer(addr);
	uint64_t& pte = pt[PT_OFFSET(va)];
	if((pte & kPteValid)) {
		addr = pte & kPteAddrMask;
	} else {
		addr = 0;
	}

	pte = value;
	return addr >> kPageShift;
}

pfn_t ArchMapKernelPage(void *ptr, pfn_t pfn, uint64_t attributes)
{
	return ArchMapPageInAddressSpace(KernelCR3, (uint64_t)ptr, pfn ? (pfn << kPageShift) | attributes | kPteValid : 0);
}
