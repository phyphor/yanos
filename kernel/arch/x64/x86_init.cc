#include "vga_console.h"

#include "arch/config.h"
#include "arch/memory_map.h"
#include "util/kprint.h"
#include "util/util_string.h"
#include "x86_structs.h"
#include "x86_util.h"

#define PML4_OFFSET(va) ((va >> 39) & 0x1FF)
#define PDP_OFFSET(va) ((va >> 30) & 0x1FF)
#define PD_OFFSET(va) ((va >> 21) & 0x1FF)
#define PT_OFFSET(va) ((va >> 12) & 0x1FF)

#define kPteAddrMask 0x000FFFFFFFFFF000
#define kPteValid 1

uint64_t KernelCR3;

namespace {
VgaConsole default_console;
TSS cpu0_tss;
GDT cpu0_gdt;
IDT cpu0_idt;

void PrintMemoryMap(const char *name, const MemoryMapEntry *e)
{
	kprintf("%s %p-%p\n", name, e->start, e->start + e->size);
}

struct [[gnu::packed]] PseudoDescriptor
{
	uint16_t limit;
	void *base;
};

void InitGDT(GDT *gdt, TSS *tss)
{
	memset(tss, 0, sizeof(*tss));

	// Here be magic numbers!
	gdt->null_descriptor = 0;
	gdt->kernel_code =  0x0020980000000000;
	gdt->kernel_data =  0x000F920000000000;
	gdt->user_code_64 = 0x0020F80000000000;
	gdt->user_code_32 = 0x00CFFB000000FFFF;
	gdt->user_data =    0x00CFF4000000FFFF;

	// Due to a quirk in the design of the TSS structure, the main fields are
	// not 64-bit aligned. We get around this by adding a 32-bit value at the
	// start, then ignoring it and giving the processor an internal pointer.
	uint64_t tss_addr = (uint64_t)&tss->reserved;
	gdt->tss_low =
		((tss_addr << 16) & 0x000000FFFFFF0000) |
		((tss_addr << 32) & 0xFF00000000000000) |
		0x0000890000000000 | (sizeof(TSS) - 4);
	gdt->tss_high = (uint32_t)(tss_addr >> 32);

	PseudoDescriptor dt;
	dt.base = gdt;
	dt.limit = sizeof(*gdt);
	__asm("lgdt %0"::"m"(dt));
	__asm("ltr %%ax"::"a"(kGdtSelTss));
	__asm("mov %%ax, %%ds; mov %%ax, %%es; mov %%ax, %%fs; mov %%ax, %%gs; mov %%ax, %%ss"::"a"(kGdtSelKernelData));

	// Clear the LDT.
	dt.base = 0;
	dt.limit = 0;
	__asm("lldt %0"::"m"(dt));
}

void InitIDT(IDT *idt)
{
	memset(idt, 0, sizeof(*idt));
	PseudoDescriptor dt;
	dt.base = idt;
	dt.limit = sizeof(*idt);
	__asm("lidt %0"::"m"(dt));
}

uint64_t* GetOrAllocateTable(uint64_t *e, uint64_t (*alloc_page)())
{
	if(!(*e & kPteValid)) {
		uint64_t paddr = alloc_page();
		memset((void*)paddr, 0, kPageSize);
		*e = paddr | kMemKernelReadWriteExec | kPteValid;
	}
	uint64_t addr = *e & kPteAddrMask;
	return (uint64_t*)addr;
}

// This provides a 1:1 mapping for physical memory, which is really useful for arbitrary memory access.
// The memory is mapped at the ASLRd address
void x86MapPhysicalMemory(uint64_t *pml4, uint64_t max_phys_addr, uint64_t (*alloc_page)())
{
	uint64_t va = PhysicalMemoryMap.start;

	if(max_phys_addr > PhysicalMemoryMap.size) {
		kprintf("WARNING: physical memory size exceeds the mapped region size");
	}

	// This works because the low gigabyte is identity mapped still
	size_t pml4e = PML4_OFFSET(va);
	uint64_t *pdp = GetOrAllocateTable(&pml4[pml4e], alloc_page);
	size_t pdpe = PDP_OFFSET(va);
	uint64_t *pd = GetOrAllocateTable(&pdp[pdpe], alloc_page);
	size_t pde = PD_OFFSET(va);
	for(uint64_t addr = 0; addr < max_phys_addr; addr += 1ULL << 21) {
		pd[pde] = addr | kMemKernelReadWriteNX | kPteValid | 0x80;
		if(++pde == 512) {
			if(++pdpe == 512) {
				pdp = GetOrAllocateTable(&pml4[++pml4e], alloc_page);
				pdpe = 0;
			}
			pd = GetOrAllocateTable(&pdp[pdpe], alloc_page);
			pde = 0;
		}
	}
}

void x86RemapKernel(uint64_t *pml4, uint64_t va, uint64_t start, uint64_t nx_start, uint64_t writable_start, uint64_t end, uint64_t (*alloc_page)())
{
	va += start;

	size_t pml4e = PML4_OFFSET(va);
	uint64_t *pdp = GetOrAllocateTable(&pml4[pml4e], alloc_page);
	size_t pdpe = PDP_OFFSET(va);
	uint64_t *pd = GetOrAllocateTable(&pdp[pdpe], alloc_page);
	size_t pde = PD_OFFSET(va);
	uint64_t *pt = GetOrAllocateTable(&pd[pde], alloc_page);
	size_t pte = PT_OFFSET(va);

	auto write_page = [&](uint64_t phys_addr, uint64_t attributes) {
		pt[pte] = phys_addr | attributes | (phys_addr ? kPteValid : 0);
		if(++pte == 512) {
			if(++pde == 512) {
				if(++pdpe == 512) {
					pdp = GetOrAllocateTable(&pml4[++pml4e], alloc_page);
					pdpe = 0;
				}
				pd = GetOrAllocateTable(&pdp[pdpe], alloc_page);
				pde = 0;
			}
			pt = GetOrAllocateTable(&pd[pde], alloc_page);
			pte = 0;
		}
	};

	uint64_t addr = start;
	for(; addr < nx_start; addr += kPageSize)
		write_page(addr, kMemKernelReadExec);
	for(; addr < writable_start; addr += kPageSize)
		write_page(addr, kMemKernelReadNX);
	for(; addr < end; addr += kPageSize)
		write_page(addr, kMemKernelReadWriteNX);
}

}

void* x86GetPhysicalMemPtr(physical_addr_t paddr)
{
	return (void*)(PhysicalMemoryMap.start +  paddr);
}

void x86Initialize(uint8_t *relocated_base)
{
	// Enable syscall & NX
	wrmsr64(kMSR_EFER, rdmsr64(kMSR_EFER) | BIT(0) | BIT(11));

	InitGDT(&cpu0_gdt, &cpu0_tss);
	InitIDT(&cpu0_idt);

	default_console.Init(relocated_base + 0xB8000, 8);
	kprint_init(&default_console,
		[](void *context){},
		[](void *context){},
		[](void *context, const char *str, size_t len){
			((VgaConsole*)context)->Puts(str, len);
		});
	kprintf("Virtual memory map\n");
	PrintMemoryMap("Kernel ", &KernelMemoryMap);
	PrintMemoryMap("Physmem", &PhysicalMemoryMap);
}

void x86InitialRemap(uint8_t *relocated_base, uint64_t max_phys_addr, uint64_t (*alloc_page)())
{
	extern uint8_t NoExecStart[];
	extern uint8_t ReadOnlyEnd[];
	extern uint8_t KernelStart[];
	extern uint8_t KernelEnd[];

	// Pages we pull out here will never be tracked by the memory manager, but they will also never be
	// freed since they map the kernel and physical memory, so that's okay

	KernelCR3 = alloc_page();
	uint64_t *pml4 = (uint64_t*)KernelCR3;
	memset(pml4, 0, kPageSize);

	// The simplest way to maintain coherent kernel mappings is just to have every page table share the
	// upper 256 entries. So we pre-allocate here because these will also never be freed.
	for(int i = 256; i < 511; i++)
		GetOrAllocateTable(&pml4[i], alloc_page);

	x86MapPhysicalMemory(pml4, max_phys_addr, alloc_page);
	x86RemapKernel(pml4, (uint64_t)relocated_base, KernelStart - relocated_base, NoExecStart - relocated_base, ReadOnlyEnd - relocated_base, KernelEnd - relocated_base, alloc_page);

	const uint64_t *old_pml4 = (uint64_t*)ReadCR3();
	pml4[0] = old_pml4[0]; // Preserve low mappings for now

	// Switch to our new, shiny page table
	WriteCR3((uint64_t)pml4);
	default_console.ChangeAddress(x86GetPhysicalMemPtr(0xB8000));
}
