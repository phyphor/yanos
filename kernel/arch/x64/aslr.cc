#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include "arch/memory_map.h"

MemoryMapEntry KernelMemoryMap = {1ULL << 22, 21};
MemoryMapEntry PhysicalMemoryMap = {1ULL << 40, 44};

namespace {

static MemoryMapEntry * const SystemMemoryMap[] = {
	&KernelMemoryMap,
	&PhysicalMemoryMap,
};

constexpr uint64_t kMinimumAddress = 0xFFFF800000000000;
constexpr uint64_t kMaximumAddress = 0xFFFFFF8000000000;

// Place segments from largest to smallest to try to get the maximum randomness
MemoryMapEntry* FindNextFreeEntry(MemoryMapEntry * const *list, size_t count)
{
	MemoryMapEntry *best = 0;
	for(size_t i = 0; i < count; i++) {
		if(list[i]->start == 0 && (!best || best->size < list[i]->size))
			best = list[i];
	}
	return best;
}

// Return the map that is > addr
const MemoryMapEntry* GetNextEntry(uint64_t addr, const MemoryMapEntry * const *list, size_t count)
{
	const MemoryMapEntry *best = 0;
	for(size_t i = 0; i < count; i++) {
		if(list[i]->start > 0 && list[i]->start > addr && (!best || list[i]->start < best->start))
			best = list[i];
	}
	return best;
}

bool GetNextFree(uint64_t *base, uint64_t *end, const MemoryMapEntry * const *list, size_t count)
{
	if(*base && *end == kMaximumAddress)
		return false;

	const MemoryMapEntry *occupied = GetNextEntry(*base, list, count);
	if(!occupied) {
		*base = kMinimumAddress;
		*end = kMaximumAddress;
		return true;
	}

	if(!*base && occupied->start != kMinimumAddress) {
		*base = kMinimumAddress;
		*end = occupied->start;
		return true;
	}

	for(;;) {
		const MemoryMapEntry *next = GetNextEntry(occupied->start, list, count);
		*base = occupied->start + occupied->size;
		if(!next) {
			*end = kMaximumAddress;
			return *base != kMaximumAddress;
		}

		*end = next->start;
		if(*base == *end) {
			occupied = next;
			continue;
		}
		return true;
	}
}

size_t CountPossibilities(uint64_t start, uint64_t end, const MemoryMapEntry *mmap)
{
	uint64_t align = 1ULL << mmap->alignment_bits;
	uint64_t mask = align - 1;
	uint64_t adjusted_start = (start + mask) & ~mask;
	if(adjusted_start + mmap->size > end)
		return 0;
	size_t size = end - (adjusted_start + mmap->size);
	return size / align;
}

void AslrPlaceRegion(MemoryMapEntry *e, uint64_t (*rng)(uint64_t))
{
	size_t count = sizeof(SystemMemoryMap) / sizeof(SystemMemoryMap[0]);

	// Try to place this entry. First figure out how many possible legal placements there are.
	// Iterate through all free address segments, adjust the start position to be aligned and
	// determine how many legal start positions there are
	// (end - start - segment_size) / segment_alignment
	size_t possibilities = 0;
	uint64_t start = 0, end = 0;
	while(GetNextFree(&start, &end, SystemMemoryMap, count))
		possibilities += CountPossibilities(start, end, e);

	// Possibilities is the number of unique legal placement locations, so
	// generate a random number from 0 to that.
	uint64_t rnd = rng(possibilities);
	start = 0;
	end = 0;
	possibilities = 0;
	// Now iterate through the list again, placing the segment in the correct position when we hit it
	while(GetNextFree(&start, &end, SystemMemoryMap, count)) {
		size_t count = CountPossibilities(start, end, e);
		if(possibilities + count >= rnd) {
			uint64_t align = 1ULL << e->alignment_bits;
			uint64_t mask = align - 1;
			uint64_t adjusted_start = (start + mask) & ~mask;
			uint64_t addr = adjusted_start + align * (rnd - possibilities);
			e->start = addr;
			break;
		}
		possibilities += count;
	}
}

void AslrAllocate(uint64_t (*rng)(uint64_t))
{
	size_t count = sizeof(SystemMemoryMap) / sizeof(SystemMemoryMap[0]);
	MemoryMapEntry *e;
	while((e = FindNextFreeEntry(SystemMemoryMap, count)))
		AslrPlaceRegion(e, rng);
}

}

extern "C" uint64_t AslrInitialize(uint64_t *page_tables)
{
	auto rng = [](uint64_t) -> uint64_t { return 0; };
	// Let the kernel get the maximum available space before the big regions eat it up
	AslrPlaceRegion(&KernelMemoryMap, rng);
	AslrAllocate(rng);
	page_tables[256] = page_tables[0];
	return KernelMemoryMap.start;
}
