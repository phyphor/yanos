#ifndef VGA_CONSOLE_H_
#define VGA_CONSOLE_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

typedef enum
{
	VgaBlack, VgaBlue, VgaGreen, VgaCyan, VgaRed, VgaMagenta, VgaYellow, VgaWhite
} VgaColor;

inline uint16_t VgaMakeColor(VgaColor fg, VgaColor bg)
{
	return ((uint16_t)fg | ((uint16_t)bg << 4)) << 8;
}

inline uint16_t VgaMakeColorFlags(VgaColor fg, VgaColor bg, bool bright, bool effect)
{
	return ((uint16_t)fg | ((uint16_t)bg << 4) | (bright ? 8 : 0) | (effect ? 0x80 : 0)) << 8;
}


class VgaConsole
{
public:
	void Init(void *data, unsigned int tabstop);
	void ChangeAddress(void *data) { data_ = (uint16_t*)data; }
	void Clear();
	void Scroll(size_t lines);
	void Puts(const char *str);
	void Puts(const char *str, size_t len);
	void Lock();
	void Unlock();

	uint16_t SetColors(uint16_t color)
	{
		uint16_t old = attrib_;
		attrib_ = color & 0xFF00;
		return old;
	}

private:
	void Putc(char ch);

	uint16_t *data_;
	uint16_t attrib_;
	size_t position_;
	unsigned int tabstop_;
};

#endif
