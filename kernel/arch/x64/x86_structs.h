#ifndef X86_STRUCTS_H_
#define X86_STRUCTS_H_

#include <stdint.h>

struct TSS
{
	uint32_t alignment; // By default the TSS isn't 8-byte aligned
	uint32_t reserved; // This is the start of the processor's TSS view
	uint64_t ring0_stack;
	uint64_t other_stack_pointers[2];
	uint64_t reserved2;
	uint64_t ist[7];
	uint64_t reserved3;
	uint64_t ioperm_offset;
};

struct GDT
{
	uint64_t null_descriptor;
	uint64_t kernel_code;
	uint64_t kernel_data;
	uint64_t user_code_32;
	uint64_t user_data;
	uint64_t user_code_64;
	uint64_t tss_low;
	uint64_t tss_high;
};

struct IDTEntry
{
	uint64_t low, high;
};

struct IDT
{
	struct IDTEntry entries[256];
};

enum InterruptType {
	kIntrInterrupt = 0x8E00,
	kIntrTrap = 0x8F00,
};

// These are chosen because the offsets are defined by the hardware.
// 0 - Always invalid
// 1 - STAR.SYSCALL_CS - kernel code segment, must be 64-bit ring0 code
// 2 - STAR.SYSCALL_CS+1 - kernel stack segment, must be 64-bit ring0 data
// 3 - STAR.SYSRET_CS - loaded on return to 32-bit, must be 32-bit ring3 code
// 4 - STAR.SYSRET_CS+1 - loaded on return to userspace, must be ring3 data
// 5 - STAR.SYSRET_CS+2 - loaded on return to 64-bit, must be 64-bit ring3 code
#define MakeSelector(x) (x << 3)

#define kGdtNull 0
#define kGdtSelNull MakeSelector(kGdtNull)

#define kGdtKernelCode 1
#define kGdtSelKernelCode MakeSelector(kGdtKernelCode)

#define kGdtKernelData 2
#define kGdtSelKernelData MakeSelector(kGdtKernelData)

#define kGdtUserCode32 3
#define kGdtSelUserCode32 MakeSelector(kGdtUserCode32) | 3

#define kGdtUserData 4
#define kGdtSelUserData MakeSelector(kGdtUserData) | 3

#define kGdtUserCode64 5
#define kGdtSelUserCode64 MakeSelector(kGdtUserCode64) | 3

#define kGdtTss 6
#define kGdtSelTss MakeSelector(kGdtTss)


#endif
