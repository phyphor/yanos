#include <stddef.h>
#include <stdint.h>

typedef uint64_t Elf64_Addr;
typedef uint64_t Elf64_Off;
typedef uint64_t Elf64_Xword;
typedef int64_t  Elf64_Sxword;
typedef uint32_t Elf64_Word;
typedef int32_t  Elf64_SWord;
typedef uint16_t Elf64_Half;


typedef struct
{
	unsigned char   e_ident[16];       /* ELF identification */
	Elf64_Half         e_type;              /* Object file type */
	Elf64_Half         e_machine;       /* Machine type */
	Elf64_Word       e_version;         /* Object file version */
	Elf64_Addr        e_entry;             /* Entry point address */
	Elf64_Off           e_phoff;             /* Program header offset */
	Elf64_Off           e_shoff;             /* Section header offset */
	Elf64_Word       e_flags;             /* Processor-specific flags */
	Elf64_Half         e_ehsize;           /* ELF header size */
	Elf64_Half         e_phentsize;      /* Size of program header entry */
	Elf64_Half         e_phnum;          /* Number of program header entries */
	Elf64_Half         e_shentsize;      /* Size of section header entry */
	Elf64_Half         e_shnum;          /* Number of section header entries */
	Elf64_Half         e_shstrndx;       /* Section name string table index */
} Elf64_Ehdr;

typedef struct
{
	Elf64_Word       sh_name;          /* Section name */
	Elf64_Word       sh_type;            /* Section type */
	Elf64_Xword      sh_flags;            /* Section attributes */
	Elf64_Addr        sh_addr;            /* Virtual address in memory */
	Elf64_Off           sh_offset;          /* Offset in file */
	Elf64_Xword      sh_size;             /* Size of section */
	Elf64_Word       sh_link;              /* Link to other section */
	Elf64_Word       sh_info;             /* Miscellaneous information */
	Elf64_Xword      sh_addralign;    /* Address alignment boundary */
	Elf64_Xword      sh_entsize;        /* Size of entries, if section has table */
} Elf64_Shdr;

typedef struct
{
	Elf64_Word       st_name;           /* Symbol name */
	unsigned char   st_info;              /* Type and Binding attributes */
	unsigned char   st_other;            /* Reserved */
	Elf64_Half         st_shndx;           /* Section table index */
	Elf64_Addr        st_value;            /* Symbol value */
	Elf64_Xword      st_size;              /* Size of object (e.g., common) */
} Elf64_Sym;

typedef struct
{
	Elf64_Addr        r_offset;             /* Address of reference */
	Elf64_Xword      r_info;                /* Symbol index and type of relocation */
} Elf64_Rel;
typedef struct
{
	Elf64_Addr        r_offset;             /* Address of reference */
	Elf64_Xword      r_info;                /* Symbol index and type of relocation */
	Elf64_Sxword    r_addend;          /* Constant part of expression */
} Elf64_Rela;

typedef struct
{
	Elf64_Word       p_type;              /* Type of segment */
	Elf64_Word       p_flags;             /* Segment attributes */
	Elf64_Off           p_offset;            /* Offset in file */
	Elf64_Addr        p_vaddr;            /* Virtual address in memory */
	Elf64_Addr        p_paddr;            /* Reserved */
	Elf64_Xword      p_filesz;             /* Size of segment in file */
	Elf64_Xword      p_memsz;         /* Size of segment in memory */
	Elf64_Xword      p_align;             /* Alignment of segment */
} Elf64_Phdr;

typedef struct
{
	Elf64_Sxword    d_tag;
	union {
		Elf64_Xword      d_val;
		Elf64_Addr        d_ptr;
	} d_un;
} Elf64_Dyn;

#define R_AMD64_RELATIVE 8

/* These constants are for the segment types stored in the image headers */
#define PT_NULL    0
#define PT_LOAD    1
#define PT_DYNAMIC 2
#define PT_INTERP  3
#define PT_NOTE    4
#define PT_SHLIB   5
#define PT_PHDR    6
#define PT_TLS     7            /* Thread local storage segment */

#define	DT_NULL		0	/* last entry in list */
#define	DT_RELA		7	/* addr of relocation entries */
#define	DT_RELASZ	8	/* size of relocation table */
#define	DT_RELAENT	9	/* base size of relocation entry */
#define	DT_REL		17	/* addr of relocation entries */
#define	DT_RELSZ	18	/* size of relocation table */
#define	DT_RELENT	19	/* base size of relocation entry */
#define DT_RELACOUNT 0x6ffffff9

#define ELF64_R_SYM(info)             ((info)>>32)
#define ELF64_R_TYPE(info)            ((Elf64_Word)(info))
#define ELF64_R_INFO(sym, type)       (((Elf64_Xword)(sym)<<32) + (Elf64_Xword)(type))

namespace {
void ElfResolve(uint64_t image_base, Elf64_Rela *rel)
{
	switch(ELF64_R_TYPE(rel->r_info)) {
	case R_AMD64_RELATIVE:
		*(Elf64_Addr*)(image_base + rel->r_offset) = image_base + rel->r_addend;
		break;
	default:
		__asm("hlt");
	}
}

void ElfResolveDynamicTable(uint64_t image_base, Elf64_Dyn *entry)
{
	constexpr size_t kArraySize = DT_RELAENT + 1;
	Elf64_Dyn *common[kArraySize];
	uint64_t count = 0;
	for(size_t i = 0; i < kArraySize; i++)
		common[i] = nullptr;

	for(; entry->d_tag != DT_NULL; entry++) {
		if(entry->d_tag >= 0 && entry->d_tag < kArraySize)
			common[entry->d_tag] = entry;
		else if(entry->d_tag == DT_RELACOUNT)
			count = entry->d_un.d_val;
	}
	if(common[DT_RELA]) {
		uint64_t base = image_base + common[DT_RELA]->d_un.d_ptr;
		for(uint64_t offset = 0; offset < common[DT_RELASZ]->d_un.d_val; offset += common[DT_RELAENT]->d_un.d_val) {
			Elf64_Rela *rel = (Elf64_Rela*)(base + offset);
			ElfResolve(image_base, rel);
		}
		if(common[DT_RELASZ]->d_un.d_val == 0) {
			for(uint64_t i = 0; i < count; i++) {
				Elf64_Rela *rel = (Elf64_Rela*)(base + common[DT_RELAENT]->d_un.d_val * i);
				ElfResolve(image_base, rel);
			}
		}
	}
}
}

extern "C" void ElfResolveRelocations(uint64_t image_base)
{
	Elf64_Ehdr *elf = reinterpret_cast<Elf64_Ehdr*>(image_base);
	uint64_t headers = image_base + elf->e_phoff;
	for(unsigned i = 0; i < elf->e_phnum; i++, headers += elf->e_phentsize) {
		Elf64_Phdr *header = reinterpret_cast<Elf64_Phdr*>(headers);
		if(header->p_type == PT_DYNAMIC) {
			ElfResolveDynamicTable(image_base, (Elf64_Dyn*)(image_base + header->p_offset));
			return;
		}
	}
}
