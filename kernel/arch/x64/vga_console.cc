#include "vga_console.h"

#include "util/util_string.h"

#define kLineWidth 80
#define kLines 25
#define kMaxPosition kLineWidth * kLines

void VgaConsole::Init(void *ptr, unsigned int tabstop)
{
	data_ = (uint16_t*)ptr;
	attrib_ = VgaMakeColor(VgaWhite, VgaBlack);
	position_ = 0;
	tabstop_ = tabstop;
	Clear();
}

void VgaConsole::Lock()
{
}

void VgaConsole::Unlock()
{
}

void VgaConsole::Clear()
{
	memset(data_, 0, kMaxPosition * 2);
}

void VgaConsole::Scroll(size_t lines)
{
	if (lines >= kLines) {
		Clear();
		return;
	}
	size_t scrolled_lines = kLines - lines;
	memmove(data_, data_ + lines * kLineWidth, scrolled_lines * kLineWidth * 2);
	memset(data_ + scrolled_lines * kLineWidth, 0, lines * kLineWidth * 2);
}

void VgaConsole::Putc(char ch)
{
	// Bochs port e9 hack - will send this char to the console
	__asm("out %%al, %%dx"::"a"(ch), "d"(0xe9));
	switch(ch) {
	case '\0':
		return;
	case '\r':
		position_ -= position_ % kLineWidth;
		break;
	case '\n':
		position_ = (position_ / kLineWidth + 1) * kLineWidth;
		break;
	case '\t':
		position_ += tabstop_ - position_ % tabstop_;
		break;
	default:
		data_[position_++] = (uint16_t)ch | attrib_;
		break;
	}
	if(position_ == kMaxPosition) {
		position_ -= kLineWidth;
		Scroll(1);
	}
}

void VgaConsole::Puts(const char *str, size_t len)
{
	for(size_t i = 0; i < len; i++)
		Putc(str[i]);
}

void VgaConsole::Puts(const char *str)
{
	char ch;
	while((ch = *str++))
		Putc(ch);
}
