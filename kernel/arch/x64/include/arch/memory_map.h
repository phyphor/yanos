#ifndef ARCH_MEMORY_MAP_H_
#define ARCH_MEMORY_MAP_H_

#include "config.h"

#define kMemFlagNX (1ULL << 63)

#define kMemKernelReadExec 0
#define kMemKernelReadWriteExec 2
#define kMemUserReadExec 4
#define kMemUserReadWriteExec 6

#define kMemKernelReadNX (kMemFlagNX | kMemKernelReadExec)
#define kMemKernelReadWriteNX (kMemFlagNX | kMemKernelReadWriteExec)
#define kMemUserReadNX (kMemFlagNX | kMemUserReadExec)
#define kMemUserReadWriteNX (kMemFlagNX | kMemUserReadWriteExec)

#define kMemCacheDisable 0x18
#define kMemGlobalPage 0x100

struct MemoryMapEntry
{
	size_t size;
	size_t alignment_bits;
	uintptr_t start;
};

extern MemoryMapEntry KernelMemoryMap;
// This provides a 1:1 physical to virtual memory mapping
extern MemoryMapEntry PhysicalMemoryMap;

#endif
