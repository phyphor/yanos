#ifndef ARCH_PHYSICAL_MEMORY_H_
#define ARCH_PHYSICAL_MEMORY_H_

#include "config.h"
#include "memory_map.h"

pfn_t ArchMapKernelPage(void *ptr, pfn_t pfn, uint64_t attributes);

// The Get functions only return pointers that are permanently mapped
inline void* ArchGetPhysicalPointer(physical_addr_t addr)
{
	if(addr > PhysicalMemoryMap.size)
		return nullptr;
	return (void*)(PhysicalMemoryMap.start + addr);
}

inline void* ArchGetPfnPointer(pfn_t pfn)
{
	return ArchGetPhysicalPointer(static_cast<physical_addr_t>(pfn) << kPageShift);
}

// This maps |pfn| to some address, performs any necessary invalidations and returns the address
template<class T = void>
inline T* ArchMapTemporaryPfn(pfn_t pfn) { return static_cast<T*>(ArchGetPfnPointer(pfn)); }

// This undoes ArchMapTemporaryPfn
inline void ArchUnmapTemporaryPfn(void *mem, pfn_t pfn) {}

#endif
