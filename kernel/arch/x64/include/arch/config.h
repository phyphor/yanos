#ifndef X64_ARCH_CONFIG_H_
#define X64_ARCH_CONFIG_H_

#include <stdint.h>
#include <stddef.h>

#define kPageShift 12
#define kPageSize (1ULL << (kPageShift))
#define kPageMask ((kPageSize) - 1) // Extracts the offset into the page
#define kPageInvMask (~kPageMask) // Clears the offset

typedef uint64_t physical_addr_t;
typedef uint32_t pfn_t;

#define ArchInvalidateMemory(ptr) __asm("invlpg %0"::"m"(*(int*)ptr));

#define DISABLE_INTERRUPTS(context) __asm volatile("pushfq; cli; pop %0; and $0x100, %0":"=r"(context))
#define RESTORE_INTERRUPTS(context) __asm volatile("and $0x100, %0; jz 1f; sti; 1:"::"r"(context))
#define ENABLE_INTERRUPTS() __asm volatile("sti")

typedef uint64_t int_status_t;

struct ScopedInterruptDisable
{
	ScopedInterruptDisable()
	{
		DISABLE_INTERRUPTS(state);
	}

	~ScopedInterruptDisable()
	{
		RESTORE_INTERRUPTS(state);
	}

private:
	int_status_t state;
};

#endif
