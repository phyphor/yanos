#ifndef X86_INIT_H_
#define X86_INIT_H_

void x86Initialize(uint8_t *relocated_base);
void x86InitialRemap(uint8_t *relocated_base, uint64_t max_phys_addr, uint64_t (*alloc_page)());

#endif
