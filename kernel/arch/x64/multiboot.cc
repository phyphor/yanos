#include <stddef.h>
#include <stdint.h>

#include "arch/config.h"
#include "core/physical_memory.h"
#include "util/kprint.h"
#include "x86_init.h"

#define MULTIBOOT_BOOT_MAGIC 0x2BADB002

extern uint8_t KernelEnd[];

namespace {

struct [[gnu::packed]] MultibootMemmap
{
	uint32_t size;
	uint64_t base_addr;
	uint64_t length;
	uint32_t type;

	MultibootMemmap* Next()
	{
		return (MultibootMemmap*)((char*)this + size + 4);
	}
};

struct MultibootInfo
{
	uint32_t flags;
	uint32_t mem_lower;
	uint32_t mem_upper;
	uint32_t boot_device;
	uint32_t cmdline;
	uint32_t mods_count;
	uint32_t mods_addr;
	uint32_t sections[4];
	uint32_t mmap_length;
	uint32_t mmap_addr;

	MultibootMemmap* FirstMMap()
	{
		return (MultibootMemmap*)(size_t)mmap_addr;
	}
};

template<class Fn>
void IterateMemoryMap(MultibootInfo *multiboot_info, const Fn& fn)
{
	if ((multiboot_info->flags & 0x40) != 0) {
		MultibootMemmap *mmap_first = multiboot_info->FirstMMap();
		for(MultibootMemmap *map = mmap_first;
		  (char*)map - (char*)mmap_first < multiboot_info->mmap_length;
		  map = map->Next()) {
			fn(map->base_addr, map->base_addr + map->length, map->type);
		}
	} else if ((multiboot_info->flags & 1) != 0) {
		// If not present, mem_upper contains the number of kB past 1 MB
		size_t size = ((size_t)multiboot_info->mem_upper) << 10;
		fn(0x100000, 0x100000 + size, 1);
	}
}

struct MemoryMapEntry
{
	uint64_t start, end;
} GlobalMemoryMap[64];
size_t GlobalMemoryMapCount = 0;

void AddFreeMemoryRange(uint64_t start, uint64_t end)
{
	GlobalMemoryMap[GlobalMemoryMapCount].start = (start + kPageMask) & kPageInvMask;
	GlobalMemoryMap[GlobalMemoryMapCount].end = end & kPageInvMask;
	GlobalMemoryMapCount++;
}

uint64_t AllocateFromMap()
{
	for(size_t i = 0; i < GlobalMemoryMapCount; i++) {
		auto& e = GlobalMemoryMap[i];
		if(e.start < e.end) {
			uint64_t ret = e.start;
			e.start += kPageSize;
			return ret;
		}
	}
	return 0;
}

}

extern "C" void MultibootEntry(MultibootInfo *multiboot_info, uint64_t multiboot_id,
                               uint64_t image_base, uint8_t *relocated_base)
{
	x86Initialize(relocated_base);

	uint64_t physical_memory_max = 0;
	kprintf("Physical memory\n");
	IterateMemoryMap(multiboot_info, [&physical_memory_max, relocated_base](uint64_t base, uint64_t end, int type) {
		if(end > physical_memory_max)
			physical_memory_max = end;
		kprintf("%p-%p type %d; %u MB\n", base, end, type, (end - base) >> 20);
		if(type == 1) {
			uint64_t kend = KernelEnd - relocated_base;
			if(base < kend)
				base = kend;
			if(base >= end)
				return;
			AddFreeMemoryRange(base, end);
		}
	});
	x86InitialRemap(relocated_base, physical_memory_max, &AllocateFromMap);

	for(size_t i = 0; i < GlobalMemoryMapCount; i++) {
		MemAddMemory(GlobalMemoryMap[i].start, GlobalMemoryMap[i].end, false);
	}
	MemInit();
}
