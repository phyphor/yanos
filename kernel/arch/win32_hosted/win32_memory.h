#ifndef WIN32_MEMORY_H_
#define WIN32_MEMORY_H_

#include <stddef.h>

bool Win32InitMemory(size_t physmem_size, bool is_test);

#endif
