#include "arch/config.h"
#include "arch/hosted.h"
#include "arch/memory_map.h"
#include "core/physical_memory.h"

#include <Windows.h>
#include <map>
#include <string.h>

namespace {

class MappedMemory
{
public:
	MappedMemory(size_t size) : Size(size), MappingObject(NULL), Memory(nullptr)
	{
		InitializeCriticalSection(&Lock);
	}
	~MappedMemory()
	{
		if(Memory)
			UnmapViewOfFile(Memory);
		for(auto& it: Mappings)
			UnmapViewOfFile((void*)it.first);
		if(MappingObject)
			CloseHandle(MappingObject);
		DeleteCriticalSection(&Lock);
	}

	bool Initialize()
	{
		uint64_t sz = Size;
		MappingObject = CreateFileMapping(INVALID_HANDLE_VALUE, NULL, PAGE_EXECUTE_READWRITE, sz >> 32, (uint32_t)sz, NULL);
		if(!MappingObject)
			return false;
		Memory = (uint8_t*)MapViewOfFile(MappingObject, FILE_MAP_WRITE, 0, 0, (SIZE_T)Size);
		return Memory != nullptr;
	}

	void Protect(physical_addr_t page_addr, DWORD prot)
	{
		AssertInRange(page_addr);
		DWORD old;
		VirtualProtect(Memory + page_addr, 1, prot, &old);
	}

	void AssertInRange(physical_addr_t end)
	{
		if(end >= Size)
			__debugbreak();
	}

	void* Map(physical_addr_t base, physical_addr_t& old, intptr_t src, DWORD prot)
	{
		if((base & kPageMask) != 0 || (src & kPageMask) != 0)
			__debugbreak();
		AssertInRange(base);
		void *p = (void*)src;
		EnterCriticalSection(&Lock);
		if(src) {
			auto iter = Mappings.find(src);
			if(iter != Mappings.end()) {
				old = iter->second;
				iter->second = base;
				UnmapViewOfFile(p);
				if(MapPage(prot, base, p) != p) {
					__debugbreak();
				}
			} else {
				p = MapPage(prot, base, p);
				Mappings[(intptr_t)p] = base;
				old = 0;
			}
		} else {
			p = MapPage(prot, base, nullptr);
			Mappings[(intptr_t)p] = base;
			old = 0;
		}
		LeaveCriticalSection(&Lock);
		return p;
	}

	void* AllocVMemPage()
	{
		physical_addr_t old;
		return Map(0, old, 0, PAGE_NOACCESS);
	}

	void* AllocVMemSized(size_t sz)
	{
		if(sz <= kPageSize)
			return AllocVMemPage();
		return MapMultiple(0, (sz + kPageSize - 1) / kPageSize, 0, PAGE_NOACCESS);
	}

	physical_addr_t FreeVMemPage(void *addr)
	{
		physical_addr_t ret = 0;
		EnterCriticalSection(&Lock);
		UnmapViewOfFile(addr);
		{
			auto it = Mappings.find((intptr_t)addr);
			if(it != Mappings.end()) {
				ret = it->second;
				Mappings.erase(it);
			}
		}
		LeaveCriticalSection(&Lock);
		return ret;
	}

	void* MapMultiple(physical_addr_t base, size_t page_count, intptr_t src, DWORD prot)
	{
		EnterCriticalSection(&Lock);
		// Windows is picky about this and exactly where the mappings go
		// So we get the mapping allocator to give us a suitably sized chunk of memory
		// It is then unmapped and the pages are individually mapped
		void *addr = MapViewOfFile(MappingObject, FILE_MAP_ALL_ACCESS, 0, 0, (SIZE_T)page_count * kPageSize);
		UnmapViewOfFile(addr);
		src = (intptr_t)addr;
		for(size_t i = 0; i < page_count; i++, base += kPageSize, src += kPageSize) {
			physical_addr_t old;
			void *p = Map(base, old, src, prot);
			if(!p)
				__debugbreak();
		}
		LeaveCriticalSection(&Lock);
		return addr;
	}

	uint8_t* ptr() { return Memory; }
	physical_addr_t size() { return Size; }

private:
	void* MapPage(DWORD prot, physical_addr_t at, void *addr)
	{
		void *ptr = MapViewOfFileEx(MappingObject, FILE_MAP_ALL_ACCESS, at >> 32, (DWORD)at, kPageSize, addr);
		if(!ptr) {
			DWORD err = GetLastError();
			err = 0;
		}
		DWORD oldprot;
		VirtualProtect(ptr, kPageSize, prot, &oldprot);
		return ptr;
	}
	HANDLE MappingObject;
	uint8_t *Memory;
	physical_addr_t Size;
	CRITICAL_SECTION Lock;
	std::map<intptr_t, physical_addr_t> Mappings;
};

MappedMemory *PhysicalMemoryObj;

}

MemoryMapEntry KernelMemoryMap = {};
MemoryMapEntry PhysicalMemoryMap = {};

pfn_t ArchMapKernelPage(void *ptr, pfn_t pfn, uint64_t attributes)
{
	if(!attributes || pfn == 0)
		attributes = PAGE_NOACCESS;
	physical_addr_t old;
	PhysicalMemoryObj->Map(pfn << kPageShift, old, (intptr_t)ptr, (DWORD)attributes);
	return (pfn_t)(old >> kPageShift);
}

bool HostInitializePhysicalMemory(size_t physmem_size, bool memset_to_junk, bool assume_clean)
{
	if(physmem_size == 0 && PhysicalMemoryObj)
		physmem_size = PhysicalMemoryObj->size();

	SYSTEM_INFO info;
	GetSystemInfo(&info);
	if(info.dwAllocationGranularity != kPageSize)
		__debugbreak();

	MemCleanup();
	delete PhysicalMemoryObj;
	PhysicalMemoryObj = new MappedMemory(physmem_size);
	if(!PhysicalMemoryObj->Initialize())
		return false;
	if(memset_to_junk)
		memset(PhysicalMemoryObj->ptr(), 0xEFEFEFEF, (size_t)PhysicalMemoryObj->size());
	KernelMemoryMap.size = 0x200000;
	PhysicalMemoryMap.size = physmem_size;
	PhysicalMemoryMap.start = (uintptr_t)PhysicalMemoryObj->ptr();

	MemAddMemory(0x200000 >> kPageShift, (pfn_t)(physmem_size >> kPageShift), assume_clean);
	MemInit();
	return true;
}
