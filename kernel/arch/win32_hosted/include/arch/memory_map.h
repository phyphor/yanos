#ifndef ARCH_MEMORY_MAP_H_
#define ARCH_MEMORY_MAP_H_

#include "config.h"

// Pulled from winnt.h
#define kMemKernelReadExec 0x20
#define kMemKernelReadWriteExec 0x40
#define kMemUserReadExec kMemKernelReadExec
#define kMemUserReadWriteExec kMemKernelReadWriteExec

#define kMemKernelReadNX 2
#define kMemKernelReadWriteNX 4
#define kMemUserReadNX kMemKernelReadNX
#define kMemUserReadWriteNX kMemKernelReadWriteNX

#define kMemCacheDisable 0x200
#define kMemGlobalPage 0

struct MemoryMapEntry
{
	size_t size;
	size_t alignment_bits;
	uintptr_t start;
};

extern MemoryMapEntry KernelMemoryMap;
// This provides a 1:1 physical to virtual memory mapping
extern MemoryMapEntry PhysicalMemoryMap;

#endif
