#ifndef WIN32_ARCH_CONFIG_H_
#define WIN32_ARCH_CONFIG_H_

#include <stdint.h>
#include <stddef.h>

// Windows typically allocates in 64kB chunks
#define kPageShift 16
#define kPageSize (1ULL << (kPageShift))
#define kPageMask ((kPageSize) - 1) // Extracts the offset into the page
#define kPageInvMask (~kPageMask) // Clears the offset

typedef uint64_t physical_addr_t;
typedef uint32_t pfn_t;

#define ArchInvalidateMemory(ptr)

typedef uint64_t int_status_t;

void InternalDisableInterrupts(int_status_t *context);
void InternalRestoreInterrupts(int_status_t *context);

#define DISABLE_INTERRUPTS(context) InternalDisableInterrupts(&context);
#define RESTORE_INTERRUPTS(context) InternalRestoreInterrupts(&context);
#define ENABLE_INTERRUPTS() InternalRestoreInterrupts(0);

struct ScopedInterruptDisable
{
	ScopedInterruptDisable()
	{
		DISABLE_INTERRUPTS(state);
	}

	~ScopedInterruptDisable()
	{
		RESTORE_INTERRUPTS(state);
	}

private:
	int_status_t state;
};

#endif
