#ifndef WIN32_HOSTED_H_
#define WIN32_HOSTED_H_

#include <stddef.h>

bool HostInitializePhysicalMemory(size_t size, bool memset_to_junk, bool assume_clean);

#endif
