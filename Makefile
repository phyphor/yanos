.PHONY: all

ifeq ($(OUT),)
	OUT:=out
endif

all: out/build.ninja
	@ninja -C $(OUT) kernel

tests: out/build.ninja
	@ninja -C $(OUT) tests

out/build.ninja: BUILD.gn
	gn gen $(OUT)
